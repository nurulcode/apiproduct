<?php

namespace App\Controller;

use App\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CategoryProductController extends AbstractController
{
    #[Route('/api/categories/product', name:'index_category_product', methods:'GET')]
    public function index(EntityManagerInterface $entityManager)
    {
        $categories = $entityManager->getRepository(Category::class)->findAll();

        $response = [];
        foreach ($categories as $category) {
            $categoryData = [
                'id' => $category->getId(),
                'name' => $category->getName(),
                'products' => [],
            ];

            foreach ($category->getProducts() as $product) {
                $categoryData['products'][] = [
                    'id' => $product->getId(),
                    'name' => $product->getName(),
                ];
            }

            $response[] = $categoryData;
        }

        return $this->json([
            'status' => true,
            'message' => 'Get data successfully',
            'data' => $response,
        ],Response::HTTP_OK);
    }
}
