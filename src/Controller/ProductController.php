<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProductController extends AbstractController
{
    #[Route('/api/product', name:'app_product', methods:'GET')]
    public function index(EntityManagerInterface $entityManager)
    {
        $categories = $entityManager->getRepository(Product::class)->findAll();

        $response = [];
        foreach ($categories as $product) {
            $response[] = [
                'id' => $product->getId(),
                'name' => $product->getName(),
                'price' => $product->getPrice(),
                'description' => $product->getDescription(),
                'category' => $product->getCategory()->getName(),
            ];
        }

        return $this->json([
            'status' => true,
            'message' => 'Get data successfully',
            'data' => $response,
        ],Response::HTTP_OK);
    }

    #[Route('/api/product', name:'store_product', methods:'POST')]
    public function store(Request $request, EntityManagerInterface $entityManager): Response
    {
        $data = json_decode($request->getContent(), true);

        $product = new Product();
        $product->setName($data['name']);
        $product->setPrice($data['price']);
        $product->setDescription($data['description']);

        $category = $entityManager->getRepository(Category::class)->find($data['category_id']);
        $product->setCategory($category);

        $entityManager->persist($product);
        $entityManager->flush();

        return $this->json([
            'status' => true,
            'message' => 'Data saved successfully',
            'data' => []
        ], Response::HTTP_CREATED);
    }


    #[Route('/api/product/{id}', name:'show_product', methods:'GET')]
    public function show(EntityManagerInterface $entityManager, $id)
    {
        $product = $entityManager->getRepository(Product::class)->find($id);

        if (!$product) {
            return $this->json([
                'status' => false,
                'message' => 'Product not found',
                'data' => [],
            ],Response::HTTP_BAD_REQUEST);
        }

        $response = [
            'id' => $product->getId(),
            'name' => $product->getName(),
            'category' => $product->getCategory()->getName(),
        ];
        
        return $this->json([
            'status' => true,
            'message' => 'Get data successfully',
            'data' => $response,
        ],Response::HTTP_OK);
    }

    #[Route('/api/product/{id}', name:'destroy_product', methods:'DELETE')]
    public function destroy(EntityManagerInterface $entityManager, $id)
    {
        $product = $entityManager->getRepository(Product::class)->find($id);

        if (!$product) {
            return $this->json([
                'status' => false,
                'message' => 'Product not found',
                'data' => [],
            ],Response::HTTP_BAD_REQUEST);
        }

        $entityManager->remove($product);
        $entityManager->flush();
        
        return $this->json([
            'status' => true,
            'message' => 'Product deleted successfully',
            'data' => [],
        ],Response::HTTP_OK);
    }
}
