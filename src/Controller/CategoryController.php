<?php

namespace App\Controller;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CategoryController extends AbstractController
{
    #[Route('/api/category', name:'app_category', methods:'GET')]
    public function index(EntityManagerInterface $entityManager)
    {
        $categories = $entityManager->getRepository(Category::class)->findAll();

        $response = [];
        foreach ($categories as $category) {
            $response[] = [
                'id' => $category->getId(),
                'name' => $category->getName(),
            ];
        }

        return $this->json([
            'status' => true,
            'message' => 'Get data successfully',
            'data' => $response,
        ],Response::HTTP_OK);
    }

    #[Route('/api/category', name:'store_category', methods:'POST')]
    public function store(Request $request, EntityManagerInterface $entityManager): Response
    {
        $data = json_decode($request->getContent(), true);
        $category = new Category();
        $category->setName($data['name']);

        $entityManager->persist($category);
        $entityManager->flush();

        return $this->json([
            'status' => true,
            'message' => 'Data saved successfully',
            'data' => []
        ], Response::HTTP_CREATED);
    }


    #[Route('/api/category/{id}', name:'show_category', methods:'GET')]
    public function show(EntityManagerInterface $entityManager, $id)
    {
        $category = $entityManager->getRepository(Category::class)->find($id);

        if (!$category) {
            return $this->json([
                'status' => false,
                'message' => 'Category not found',
                'data' => [],
            ],Response::HTTP_BAD_REQUEST);
        }

        $response = [
            'id' => $category->getId(),
            'name' => $category->getName(),
        ];
        
        return $this->json([
            'status' => true,
            'message' => 'Get data successfully',
            'data' => $response,
        ],Response::HTTP_OK);
    }

    #[Route('/api/category/{id}', name:'destroy_category', methods:'DELETE')]
    public function destroy(EntityManagerInterface $entityManager, $id)
    {
        $category = $entityManager->getRepository(Category::class)->find($id);

        if (!$category) {
            return $this->json([
                'status' => false,
                'message' => 'Category not found',
                'data' => [],
            ],Response::HTTP_BAD_REQUEST);
        }

        $entityManager->remove($category);
        $entityManager->flush();
        
        return $this->json([
            'status' => true,
            'message' => 'Category deleted successfully',
            'data' => [],
        ],Response::HTTP_OK);
    }
}
